// Fill out your copyright notice in the Description page of Project Settings.
#include "FrogBox.h"

// Sets default values
AFrogBox::AFrogBox()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    static_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("static_mesh"));
    static ConstructorHelpers::FObjectFinder<UStaticMesh> chamfer_cube_mesh(
    TEXT("/Script/Engine.StaticMesh'/Game/LevelPrototyping/Meshes/SM_ChamferCube.SM_ChamferCube'")
    );

    if (chamfer_cube_mesh.Succeeded())
    {
        static_mesh->SetStaticMesh(chamfer_cube_mesh.Object);
    }

    SetRootComponent(static_mesh);

    // create base_hitbox and attach onto the root component
    base_hitbox = CreateDefaultSubobject<UBoxComponent>(TEXT("base_hitbox"));
    base_hitbox->SetupAttachment(RootComponent);

    // extract the bounding box of the base_hitbox component
    FBox base_bounding_box = static_mesh->GetStaticMesh()->GetBoundingBox();
    double base_x = base_bounding_box.Max.X;
    double base_y = base_bounding_box.Max.Y;
    double base_height = base_bounding_box.Max.Z;
    base_hitbox->SetBoxExtent(FVector(base_x, base_y, base_height));
    base_hitbox->SetHiddenInGame(true);
    base_hitbox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    base_hitbox->SetGenerateOverlapEvents(true);

    /*
    * Questioning if this should even have collision events or if it should just
    * be used as a hitbox for face hitboxes to determine there is a box there.
    * But then wouldn't that be redundant because in theory the face hitboxes
    * can just use the base mesh model as the intersecting options.
    */

    front_hitbox = GenerateFaceHitbox(this, base_bounding_box, FaceHitboxEnum::kFront, "front_hitbox");
    front_hitbox->SetupAttachment(RootComponent);

    back_hitbox = GenerateFaceHitbox(this, base_bounding_box, FaceHitboxEnum::kBack, "back_hitbox");
    back_hitbox->SetupAttachment(RootComponent);

    right_hitbox = GenerateFaceHitbox(this, base_bounding_box, FaceHitboxEnum::kRight, "right_hitbox");
    right_hitbox->SetupAttachment(RootComponent);

    left_hitbox = GenerateFaceHitbox(this, base_bounding_box, FaceHitboxEnum::kLeft, "left_hitbox");
    left_hitbox->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AFrogBox::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void AFrogBox::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

}


void AFrogBox::HandleOverlapBegin(FaceHitboxEnum facet, AActor* other_actor)
{
    bool is_player = other_actor != this && other_actor->ActorHasTag(FName("Player"));
    bool is_box = other_actor != this && other_actor->ActorHasTag(FName("Box"));
    FString facet_string = "";
    
    // TODO: Delete later. This is only for debugging.
    switch (facet)
    {
    case(FaceHitboxEnum::kFront):
        facet_string = "FRONT";
        break;
    case(FaceHitboxEnum::kBack):
        facet_string = "BACK";
        break;
    case(FaceHitboxEnum::kRight):
        facet_string = "RIGHT";
        break;
    case(FaceHitboxEnum::kLeft):
        facet_string = "LEFT";
        break;  
    default:
        break;
    }

    if (is_player)
    {
        UE_LOG(LogTemp, Warning, TEXT("%s Player Overlap: %s"), *facet_string, *other_actor->GetName());
    }
    else if (is_box)
    {
        UE_LOG(LogTemp, Warning, TEXT("%s Box Overlap: %s"), *facet_string, *other_actor->GetName());
    }
}

void AFrogBox::HandleOverlapEnd(FaceHitboxEnum facet, AActor* other_actor)
{
    bool is_player = other_actor != this && other_actor->ActorHasTag(FName("Player"));
    bool is_box = other_actor != this && other_actor->ActorHasTag(FName("Box"));
    FString facet_string = "";

    // TODO: Delete later. This is only for debugging.
    switch (facet)
    {
    case(FaceHitboxEnum::kFront):
        facet_string = "FRONT";
        break;
    case(FaceHitboxEnum::kBack):
        facet_string = "BACK";
        break;
    case(FaceHitboxEnum::kRight):
        facet_string = "RIGHT";
        break;
    case(FaceHitboxEnum::kLeft):
        facet_string = "LEFT";
        break;
    default:
        break;
    }

    if (is_player)
    {
        UE_LOG(LogTemp, Warning, TEXT("%s Player exited: %s"), *facet_string, *other_actor->GetName());
    }
    else if (is_box)
    {
        UE_LOG(LogTemp, Warning, TEXT("%s Box exited: %s"), *facet_string, *other_actor->GetName());
    }
}

// Overlap FRONT
void AFrogBox::OnOverlapFrontBegin(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index, bool from_sweep, const FHitResult& sweep_result)
{
    HandleOverlapBegin(FaceHitboxEnum::kFront, other_actor);
}
    
void AFrogBox::OnOverlapFrontEnd(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index)
{
    HandleOverlapEnd(FaceHitboxEnum::kFront, other_actor);
}

// Overlap BACK
void AFrogBox::OnOverlapBackBegin(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index, bool from_sweep, const FHitResult& sweep_result)
{
    HandleOverlapBegin(FaceHitboxEnum::kBack, other_actor);
}
void AFrogBox::OnOverlapBackEnd(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index)
{
    HandleOverlapEnd(FaceHitboxEnum::kBack, other_actor);
}

// Overlap RIGHT
void AFrogBox::OnOverlapRightBegin(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index, bool from_sweep, const FHitResult& sweep_result)
{
    HandleOverlapBegin(FaceHitboxEnum::kRight, other_actor);
}
void AFrogBox::OnOverlapRightEnd(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index)
{
    HandleOverlapEnd(FaceHitboxEnum::kRight, other_actor);
}

// Overlap LEFT
void AFrogBox::OnOverlapLeftBegin(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index, bool from_sweep, const FHitResult& sweep_result)
{
    HandleOverlapBegin(FaceHitboxEnum::kLeft, other_actor);
}
void AFrogBox::OnOverlapLeftEnd(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index)
{
    HandleOverlapEnd(FaceHitboxEnum::kLeft, other_actor);
}

UBoxComponent* AFrogBox::GenerateFaceHitbox(AFrogBox* frog_box, FBox base_fbox, FaceHitboxEnum facet, FName name)
{
    double base_x = base_fbox.Max.X;
    double base_y = base_fbox.Max.Y;
    double base_height = base_fbox.Max.Z;
    UBoxComponent* face_hitbox = frog_box->CreateDefaultSubobject<UBoxComponent>(name);
    face_hitbox->SetHiddenInGame(true);
    face_hitbox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    face_hitbox->SetGenerateOverlapEvents(true);
    face_hitbox->SetCollisionProfileName(TEXT("OverlapAllDynamic"));

    if (facet == FaceHitboxEnum::kFront || facet == FaceHitboxEnum::kBack)
    {
        double front_back_width = base_x * 0.75;
        double front_back_depth = base_y * 0.10;
        double center_distance = 0;
        
        if (facet == FaceHitboxEnum::kFront)
        {
            center_distance = front_back_depth + base_y;
            face_hitbox->OnComponentBeginOverlap.AddDynamic(frog_box, &AFrogBox::OnOverlapFrontBegin);
            face_hitbox->OnComponentEndOverlap.AddDynamic(frog_box, &AFrogBox::OnOverlapFrontEnd);
        }
        else
        {
            center_distance = (front_back_depth + base_y) * -1;
            face_hitbox->OnComponentBeginOverlap.AddDynamic(frog_box, &AFrogBox::OnOverlapBackBegin);
            face_hitbox->OnComponentEndOverlap.AddDynamic(frog_box, &AFrogBox::OnOverlapBackEnd);
        }

        face_hitbox->SetBoxExtent(FVector(front_back_width, front_back_depth, base_height));
        face_hitbox->SetRelativeLocation(FVector(0.0, center_distance, 0.0));
    }
    else // kRight, kLeft faces
    {
        double right_left_width = base_y * 0.75;
        double right_left_depth = base_x * 0.10;
        double center_distance = 0;

        if (facet == FaceHitboxEnum::kRight)
        {
            center_distance = base_x + right_left_depth;
            face_hitbox->OnComponentBeginOverlap.AddDynamic(frog_box, &AFrogBox::OnOverlapRightBegin);
            face_hitbox->OnComponentEndOverlap.AddDynamic(frog_box, &AFrogBox::OnOverlapRightEnd);
        }
        else
        {
            center_distance = (base_x + right_left_depth) * -1;
            face_hitbox->OnComponentBeginOverlap.AddDynamic(frog_box, &AFrogBox::OnOverlapLeftBegin);
            face_hitbox->OnComponentEndOverlap.AddDynamic(frog_box, &AFrogBox::OnOverlapLeftEnd);
        }
        face_hitbox->SetBoxExtent(FVector(right_left_depth, right_left_width, base_height));
        face_hitbox->SetRelativeLocation(FVector(center_distance, 0.0, 0.0));
    }

    return face_hitbox;
}
