// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "BaseTile.generated.h"

UCLASS()
class PROJECTFROG_API ABaseTile : public AActor
{
    GENERATED_BODY()

public:	
    // Sets default values for this actor's properties
    ABaseTile();

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    FString test_message = FString(TEXT("Hello World, I'm ABaseTile yo"));

public:	
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    UPROPERTY(EditAnywhere, Category = "Mesh")
    UStaticMeshComponent* static_mesh;
    UBoxComponent* hitbox;
  
    UFUNCTION()
    void OnOverlapBegin(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index, bool from_sweep, const FHitResult& sweep_result);
    UFUNCTION()
    void OnOverlapEnd(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index);

};
