// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjectFROGGameMode.generated.h"

UCLASS(minimalapi)
class AProjectFROGGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AProjectFROGGameMode();
};



