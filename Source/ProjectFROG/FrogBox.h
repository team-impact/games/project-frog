// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "FrogBox.generated.h"


enum class FaceHitboxEnum
{
    kFront,
    kBack,
    kLeft,
    kRight,
};

UCLASS()
class PROJECTFROG_API AFrogBox : public AActor
{
    GENERATED_BODY()
    
public:
    // Sets default values for this actor's properties
    AFrogBox();

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    UBoxComponent* GenerateFaceHitbox(AFrogBox* frog_box, FBox base_hitbox, FaceHitboxEnum facet, FName name);

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    UPROPERTY(EditAnywhere, Category = "Mesh")
    UStaticMeshComponent* static_mesh;
    UBoxComponent* base_hitbox;

    // pushing side hitboxes;
    UBoxComponent* front_hitbox;
    UBoxComponent* left_hitbox;
    UBoxComponent* back_hitbox;
    UBoxComponent* right_hitbox;

    // this is kinda ugly, but for now this is how I'll do things. There's probably a better abstraction for this.
    UFUNCTION()
    void OnOverlapFrontBegin(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index, bool from_sweep, const FHitResult& sweep_result);
    UFUNCTION()
    void OnOverlapFrontEnd(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index);

    UFUNCTION()
    void OnOverlapLeftBegin(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index, bool from_sweep, const FHitResult& sweep_result);
    UFUNCTION()
    void OnOverlapLeftEnd(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index);

    UFUNCTION()
    void OnOverlapBackBegin(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index, bool from_sweep, const FHitResult& sweep_result);
    UFUNCTION()
    void OnOverlapBackEnd(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index);

    UFUNCTION()
    void OnOverlapRightBegin(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index, bool from_sweep, const FHitResult& sweep_result);
    UFUNCTION()
    void OnOverlapRightEnd(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index);

    void HandleOverlapBegin(FaceHitboxEnum facet, AActor* other_actor);
    void HandleOverlapEnd(FaceHitboxEnum facet, AActor* other_actor);
};