// Copyright Epic Games, Inc. All Rights Reserved.

#include "ProjectFROG.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ProjectFROG, "ProjectFROG" );

DEFINE_LOG_CATEGORY(LogProjectFROG)
 