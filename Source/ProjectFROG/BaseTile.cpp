// Fill out your copyright notice in the Description page of Project Settings.
#include "BaseTile.h"

// Sets default values
ABaseTile::ABaseTile()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    // basically, CreateDefaultSubobject of type of a UStaticMeshComponent named "StaticMeshComponent"
    // perhaps we can have other StaticMeshComponents made on our own?
    static_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("static_mesh"));

    static ConstructorHelpers::FObjectFinder<UStaticMesh> tile_mesh(
        TEXT("/Script/Engine.StaticMesh'/Game/LevelPrototyping/Meshes/proto_tile.proto_tile'")
    );

    if (tile_mesh.Succeeded())
    {
        static_mesh->SetStaticMesh(tile_mesh.Object);
    }

    SetRootComponent(static_mesh);

    // Initialize the hitbox
    hitbox = CreateDefaultSubobject<UBoxComponent>(TEXT("hitbox"));
    hitbox->SetupAttachment(RootComponent);

    FBox bounding_box = static_mesh->GetStaticMesh()->GetBoundingBox();
    double x_length = bounding_box.Max.X;
    double y_length = bounding_box.Max.Y;
    double z_length = bounding_box.Max.Z;
    // set box size and dimensions
    hitbox->SetBoxExtent(FVector(x_length, y_length, z_length / 2.0));

    double z_relative_height = z_length + (z_length / 2.0);
    hitbox->SetRelativeLocation(FVector(0.0, 0.0, z_relative_height));

    hitbox->SetHiddenInGame(true);           
    hitbox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    hitbox->SetGenerateOverlapEvents(true);
    hitbox->SetCollisionProfileName(TEXT("OverlapAllDynamic"));

    hitbox->OnComponentBeginOverlap.AddDynamic(this, &ABaseTile::OnOverlapBegin);
    hitbox->OnComponentEndOverlap.AddDynamic(this, &ABaseTile::OnOverlapEnd);
}

// Called when the game starts or when spawned
void ABaseTile::BeginPlay()
{
    Super::BeginPlay();

    // I have no idea what GEngine is and why we have to conditionally check if it exists first before it can do this in a BeginPlay() call
    if (GEngine)
    {
        GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Cyan, FString::Printf(TEXT("%s"), *test_message));
    }
}

// Called every frame
void ABaseTile::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

}

void ABaseTile::OnOverlapBegin(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index, bool from_sweep, const FHitResult& sweep_result)
{
    UE_LOG(LogTemp, Warning, TEXT("Actor Entered: %s"), *other_actor->GetName());
    if (GEngine)
    {
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, FString::Printf(TEXT("Actor entered: %s"), *other_actor->GetName()));
    }
}

void ABaseTile::OnOverlapEnd(UPrimitiveComponent* overlapped_component, AActor* other_actor, UPrimitiveComponent* other_comp, int32 other_body_index)
{
    UE_LOG(LogTemp, Warning, TEXT("Actor Exited: %s"), *other_actor->GetName());
    if (GEngine)
    {
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::Printf(TEXT("Actor exited: %s"), *other_actor->GetName()));
    }
}
